#!/bin/bash

VERSION=$(cat  /home/app/www/html/patch_live/ryzom_live.version | cut -d" " -f1)
VERSION=$(printf "%05.f" $VERSION)

rm -f app/data/*.bnp

function getBnp() {
	Infos=$(python get_last_versions.py /home/app/www/html/patch_live/$VERSION/ryzom_${VERSION}_debug.xml $1)
	Name=$(echo $Infos | cut -d" " -f1)
	Version=$(echo $Infos | cut -d" " -f2)
	cp /home/app/www/html/patch_live/$Version/$Name.lzma app/data/
	echo $Name
	lzma -d app/data/$Name.lzma
}


getBnp construction.bnp
getBnp data_common.bnp
getBnp leveldesign.bnp
getBnp objects.bnp
getBnp sfx.bnp
getBnp packedsheets.bnp

getBnp fauna_swt.bnp
getBnp fauna_skeletons.bnp
getBnp fauna_shapes.bnp
getBnp fauna_maps.bnp
getBnp fauna_animations.bnp

getBnp characters_swt.bnp
getBnp characters_skeletons.bnp
getBnp characters_shapes.bnp
getBnp characters_maps_zo_hom_visage_hr.bnp
getBnp characters_maps_zo_hom_underwear_hr.bnp
getBnp characters_maps_zo_hom_civil01_hr.bnp
getBnp characters_maps_zo_hom_cheveux_hr.bnp
getBnp characters_maps_zo_hom_caster01_hr.bnp
getBnp characters_maps_zo_hom_casque01_hr.bnp
getBnp characters_maps_zo_hom_armor01_hr.bnp
getBnp characters_maps_zo_hom_armor00_hr.bnp
getBnp characters_maps_zo_hof_visage_hr.bnp
getBnp characters_maps_zo_hof_underwear_hr.bnp
getBnp characters_maps_zo_hof_civil01_hr.bnp
getBnp characters_maps_zo_hof_cheveux_hr.bnp
getBnp characters_maps_zo_hof_caster01_hr.bnp
getBnp characters_maps_zo_hof_casque01_hr.bnp
getBnp characters_maps_zo_hof_armor01_hr.bnp
getBnp characters_maps_zo_hof_armor00_hr.bnp
getBnp characters_maps_tr_hom_visage_hr.bnp
getBnp characters_maps_tr_hom_underwear_hr.bnp
getBnp characters_maps_tr_hom_refugee_hr.bnp
getBnp characters_maps_tr_hom_civil01_hr.bnp
getBnp characters_maps_tr_hom_cheveux_hr.bnp
getBnp characters_maps_tr_hom_caster01_hr.bnp
getBnp characters_maps_tr_hom_casque01_hr.bnp
getBnp characters_maps_tr_hom_armor01_hr.bnp
getBnp characters_maps_tr_hom_armor00_hr.bnp
getBnp characters_maps_tr_hof_visage_hr.bnp
getBnp characters_maps_tr_hof_underwear_hr.bnp
getBnp characters_maps_tr_hof_refugee_hr.bnp
getBnp characters_maps_tr_hof_civil01_hr.bnp
getBnp characters_maps_tr_hof_cheveux_hr.bnp
getBnp characters_maps_tr_hof_caster01_hr.bnp
getBnp characters_maps_tr_hof_casque01_hr.bnp
getBnp characters_maps_tr_hof_armor01_hr.bnp
getBnp characters_maps_tr_hof_armor00_hr.bnp
getBnp characters_maps_ma_hom_visage_hr.bnp
getBnp characters_maps_ma_hom_underwear_hr.bnp
getBnp characters_maps_ma_hom_civil01_hr.bnp
getBnp characters_maps_ma_hom_cheveux_hr.bnp
getBnp characters_maps_ma_hom_caster01_hr.bnp
getBnp characters_maps_ma_hom_casque01_hr.bnp
getBnp characters_maps_ma_hom_armor01_hr.bnp
getBnp characters_maps_ma_hom_armor00_hr.bnp
getBnp characters_maps_ma_hof_visage_hr.bnp
getBnp characters_maps_ma_hof_underwear_hr.bnp
getBnp characters_maps_ma_hof_civil01_hr.bnp
getBnp characters_maps_ma_hof_cheveux_hr.bnp
getBnp characters_maps_ma_hof_caster01_hr.bnp
getBnp characters_maps_ma_hof_casque01_hr.bnp
getBnp characters_maps_ma_hof_armor01_hr.bnp
getBnp characters_maps_ma_hof_armor00_hr.bnp
getBnp characters_maps_hr.bnp
getBnp characters_maps_fy_hom_visage_hr.bnp
getBnp characters_maps_fy_hom_underwear_hr.bnp
getBnp characters_maps_fy_hom_ruflaket_hr.bnp
getBnp characters_maps_fy_hom_civil01_hr.bnp
getBnp characters_maps_fy_hom_cheveux_hr.bnp
getBnp characters_maps_fy_hom_caster01_hr.bnp
getBnp characters_maps_fy_hom_casque01_hr.bnp
getBnp characters_maps_fy_hom_barman_hr.bnp
getBnp characters_maps_fy_hom_armor01_hr.bnp
getBnp characters_maps_fy_hom_armor00_hr.bnp
getBnp characters_maps_fy_hof_visage_hr.bnp
getBnp characters_maps_fy_hof_underwear_hr.bnp
getBnp characters_maps_fy_hof_civil01_hr.bnp
getBnp characters_maps_fy_hof_cheveux_hr.bnp
getBnp characters_maps_fy_hof_caster01_hr.bnp
getBnp characters_maps_fy_hof_armor01_hr.bnp
getBnp characters_maps_fy_hof_armor00_hr.bnp
getBnp characters_animations.bnp

