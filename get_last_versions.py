#!/usr/bin/env python3

import os, sys
import subprocess
import xml.etree.ElementTree as ET

tree = ET.parse(sys.argv[1])
root = tree.getroot()
ignore = 1
for item in root.findall("_Files/_Files"):
	last_version = 0
	name = item.find("_FileName").attrib["value"]
	shortname = name[:-4]
	last_version = 0
	versions = []
	for vers in item.findall("_Versions"):
		for v in vers:
			if v.tag == "_VersionNumber":
				version = v.attrib["value"]
				last_version = version
	if len(sys.argv) <= 2 or sys.argv[2] == name:
		print("%s %05.f" % (name, int(last_version)))

